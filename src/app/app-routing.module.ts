import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { PatientComponent } from './patient/patient.component';


const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },{ path: '', component: HeaderComponent },
  { path: 'patient', component: PatientComponent},{path: 'patient', component:PatientComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [LoginComponent, PatientComponent]
