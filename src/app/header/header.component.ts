import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  myimage:string = "/assets/img/patient.jpg";
  mypc:string = "/assets/img/mac.jpg";
  constructor() { }

  ngOnInit(): void {
  }

}
